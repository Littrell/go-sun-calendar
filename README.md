# Introduction   

This is a simple Go application that calculates the sunrise and sunset from longitude and latitude.

## Build
```bash
go build
```

## Examples  

```bash
# Broomfield, CO, USA
./go-sun-cycles --lat=39.94653 --long=-105.12264 --tz=-6

# Timezone offset isn't necessary when latitude
# and longitude are in the local timezone
# The --tz option simply applies an offset
./go-sun-cycles --lat=39.94653 --long=-105.12264

# Sunset
./go-sun-cycles --lat=39.94653 --long=-105.12264 --tz=-6 --rise=false

# Specify a Unix timestamp date/time
./go-sun-cycles --lat=39.94653 --long=-105.12264 --tz=-6 --time=$(date +%s)

# Specify a julian date/time; useful for historic sunrise and sunsets.
# Julian dates are accurate and valid back to January 1, 4713 BC.
./go-sun-cycles --lat=39.94653 --long=-105.12264 --tz=-6 --time=2459502.03889 --isJulian=true
```

